<?php
/**
 * Nuclear Tuxedo Big Background slideshow.
 *
 * @package   NT Big Background Slideshow
 * @author    Colby Sollars <colby@nucleartuxedo.com>
 * @license   GPL-2.0+
 * @link      http://nucleartuxedo.com
 * @copyright 2013 Nuclear Tuxedo
 *
 * @wordpress-plugin
 * Plugin Name: NT Big Bacgkground Slideshow
 * Plugin URI:	http://plugins.nucleartuxedo.com/extend/plugins/nt-big-background-slideshow/
 * Description: Big background slideshow
 * Version:     0.0.1
 * Author:      Colby Sollars
 * Author URI:  http://nucleartuxedo.com/author/coaltown/
 * Text Domain: nt_big_background_slideshow
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'nt-big-background-slideshow.class.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'NTBigBackgroundSlideshow', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'NTBigBackgroundSlideshow', 'deactivate' ) );

NTBigBackgroundSlideshow::get_instance();