<?php
/**
 * NT Big Background Slideshow.
 *
 * @package   NTBigBackgroundSlideshow
 * @author    Colby Sollars <colby@nucleartuxedo.com>
 * @license   GPL-2.0+
 * @link      http://nucleartuxedo.com/
 * @copyright 2013 Nuclear Tuxedo
 */

/**
 * Plugin class.
 *
 * @package NTBigBackgroundSlideshow
 * @author  Colby Sollars <colby@nucleartuxedo.com>
 */
class NTBigBackgroundSlideshow {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   0.0.1
	 *
	 * @var     string
	 */
	protected $version = '0.0.1';

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'nt-big-background-slideshow';

	/**
	 * Instance of this class.
	 *
	 * @since    0.0.1
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    0.0.1
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	protected $admin_notice_message = "";

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since     0.0.1
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'nt_big_background_slideshow' ) );
		add_action( 'init', array( $this, 'check_submitted_data' ) );

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Admin notice for the flash message on save
		add_action( 'admin_notices', array( $this, 'show_admin_notice' ) );

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		add_action( 'wp_footer', array( $this, 'add_background' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    0.0.1
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
		// TODO: Define activation functionality here
	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    0.0.1
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {
		// TODO: Define deactivation functionality here
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.0.1
	 */
	public function nt_big_background_slideshow() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}

	public function check_submitted_data() {
		if(isset($_POST['_wpnonce']) and wp_verify_nonce($_POST['_wpnonce'], 'nt-big-background-slideshow')) {
			update_option('nt-big-background-slideshow_pattern_name', $_POST['pattern_name']);
			update_option('nt-big-background-slideshow_background_images', $_POST['background_images']);
			update_option('nt-big-background-slideshow_bg_slideshow_enabled', isset($_POST['bg_slideshow_enabled']));
			update_option('nt-big-background-slideshow_effect', $_POST['effect']);
			update_option('nt-big-background-slideshow_timeout', $_POST['timeout']);
			$this->admin_notice_message = "Background options updated.";
		}
	}

	public function show_admin_notice() {
		if($this->admin_notice_message)
			echo '<div class="updated"><p>'.$this->admin_notice_message.'</p></div>';
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     0.0.1
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $screen->id == $this->plugin_screen_hook_suffix ) {
			wp_enqueue_style( $this->plugin_slug .'-maximage', plugins_url( 'css/jquery.maximage.css?v=1.2', __FILE__ ), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-maximage-screen', plugins_url( 'css/screen.css?v=1.2', __FILE__ ), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'css/admin.css', __FILE__ ), $this->version );
		}

	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     0.0.1
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $screen->id == $this->plugin_screen_hook_suffix ) {
			wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-cycle', plugins_url( 'js/jquery.cycle.all.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script('maximage', plugins_url( 'js/jquery.maximage.js', __FILE__ ), array( 'jquery-cycle' ), $this->version );
			wp_enqueue_media();
		}

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug .'-maximage', plugins_url( 'css/jquery.maximage.css?v=1.2', __FILE__ ), $this->version );
		wp_enqueue_style( $this->plugin_slug .'-maximage-screen', plugins_url( 'css/screen.css?v=1.2', __FILE__ ), $this->version );
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'css/public.css', __FILE__ ), $this->version );
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_scripts() {
		wp_enqueue_script('jquery-cycle', plugins_url( 'js/jquery.cycle.all.js', __FILE__ ), array( 'jquery' ), $this->version );
		wp_enqueue_script('maximage', plugins_url( 'js/jquery.maximage.js', __FILE__ ), array( 'jquery-cycle' ), $this->version );
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url( 'js/public.js', __FILE__ ), array( 'jquery' ), $this->version );
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    0.0.1
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Big Background Slideshow Settings', $this->plugin_slug ),
			__( 'Big Background', $this->plugin_slug ),
			'read',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);

	}

	public function add_background() {
		if(!get_option('nt-big-background-slideshow_bg_slideshow_enabled', true)) return;
		$pattern = get_option('nt-big-background-slideshow_pattern_name', array());
		$background_images = get_option('nt-big-background-slideshow_background_images', array());
		$effect = get_option('nt-big-background-slideshow_effect', 'fade');
		$maximage_effect = 'fade';
		if($effect == 'slide') $maximage_effect = 'scrollHorz';
		$timeout = get_option('nt-big-background-slideshow_timeout', 5000);
		include_once('views/background.php');
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    0.0.1
	 */
	public function display_plugin_admin_page() {
		include_once( 'views/admin.php' );
	}

}