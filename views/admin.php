<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   PluginName
 * @author    Your Name <email@example.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2013 Your Name or Company Name
 */

$patterns = scandir(plugin_dir_path( dirname(__FILE__) ) . 'assets/patterns');
$background_images = get_option('nt-big-background-slideshow_background_images', array());
$selected_pattern = get_option('nt-big-background-slideshow_pattern_name', '');
$bg_slideshow_enabled = get_option('nt-big-background-slideshow_bg_slideshow_enabled', true);
$slideshow_effect = get_option('nt-big-background-slideshow_effect', 'fade');
$maximage_effect = 'fade';
if($slideshow_effect == 'slide') $maximage_effect = "scrollHorz";
$timeout = get_option('nt-big-background-slideshow_timeout', 5000);

?>
<div class="wrap">

	<?php screen_icon(); ?>
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<form method="post" action="options-general.php?page=nt-big-background-slideshow" id="nt-bg-form">

		<br />
		<input type="checkbox" name="bg_slideshow_enabled" value="true" <?= $bg_slideshow_enabled ? ' checked="checked"' : '' ?> /> Enabled?
		<br />
		<br />
		
		<div class="uploader">
			<input class="upload_image_button button" name="" id="upload_button" value="Background Images..." />
		</div>

		<ul id="slideshow-images">
			<? foreach ($background_images as $att_id => $image) : ?>
			<li>
				<div class="thumbnail">
					<div class="centered">
						<img src="<?=$image['src']?>" class="<?=$image['orientation']?>" />
					</div>
				</div>
			</li>
			<? endforeach ?>
		</ul>
		<br style="clear:both" />

		<div id="pattern_select_wrapper">
			<ul id="slideshow-patterns">
				<li <?= $selected_pattern == '' ? ' class="selected"' : '' ?>>
					<a href="#" onmouseout="set_overlay_selected()" onclick="set_selected_pattern(this, '')" onmouseover="set_overlay_pattern('')" class="pattern-sample" style="background-image: none; background-color: #FFF">&nbsp;</a>
				</li>
				<?php foreach($patterns as $pattern) : ?>
					<?php if( substr($pattern, 0, 1) == ".") continue; ?>
					<li <?= $selected_pattern == $pattern ? ' class="selected"' : '' ?>>
						<a href="#" onclick="set_selected_pattern(this, '<?=$pattern?>')" onmouseout="set_overlay_selected()" onmouseover="set_overlay_pattern('<?=$pattern?>')" class="pattern-sample" style="background-image: url('/wp-content/plugins/nt-big-background-slideshow/assets/patterns/<?=$pattern?>')"></a>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
		<br style="clear:both" />

		<div id="preview_wrapper">
			<div id="bg-preview">
				<? foreach ($background_images as $image) : ?>
					<img src="<?= $image['src'] ?>" class="mc-image <?=$slideshow_effect?>" />
				<? endforeach ?>
			</div>
			<div id="bg-overlay" <?= $selected_pattern == '' ? '' : 'style="background-image: url(\'/wp-content/plugins/nt-big-background-slideshow/assets/patterns/'.$selected_pattern.'\')"' ?>>
				&nbsp;
			</div>
		</div>
		<br />

		Slide Effect:
		<select name="effect">
			<option value="fade"<?= $slideshow_effect == 'fade' ? ' selected="selected"' : '' ?>>Fade</option>
			<option value="slide"<?= $slideshow_effect == 'slide' ? ' selected="selected"' : '' ?>>Slide</option>
		</select>
		<br />

		<br />
		Timeout (MilliSeconds): <input type="text" size="6" name="timeout" value="<?=$timeout?>" />
		<br />

		<input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('nt-big-background-slideshow') ?>" />
		<input type="hidden" name="pattern_name" value="<?= $selected_pattern ?>" id="pattern_name_input" />
		<? foreach ($background_images as $att_id => $image) : ?>
			<input type="hidden" name="background_images[<?=$att_id?>][src]" value="<?=$image['src']?>" />
			<input type="hidden" name="background_images[<?=$att_id?>][orientation]" value="<?=$image['orientation']?>" />
		<? endforeach ?>
		<input style="margin-top: 1.4em;" type="submit" class="button button-primary" value="Save Changes" />
	</form>

</div>
<script type="text/javascript">// Uploading files
	var file_frame;

	jQuery('.upload_image_button').live('click', function( event ){

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}

		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: jQuery( this ).data( 'uploader_title' ),
			button: {
				text: jQuery( this ).data( 'uploader_button_text' ),
			},
			multiple: true  // Set to true to allow multiple files to be selected
		});

		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {

			var selection = file_frame.state().get('selection');

			jQuery('#slideshow-images>li').remove();
			jQuery('#bg-preview').remove();
			bg_prev = document.createElement('div');
			jQuery(bg_prev).attr('id', 'bg-preview');
			jQuery('#preview_wrapper').append(bg_prev);
			jQuery('input[name^=background_images]').remove();
			selection.map( function( attachment ) {

				attachment = attachment.toJSON();
				v = "";
				for(i in attachment) {
					v+= i + ": " + attachment[i] + "\n";
				}
				//alert(v);

				// Set up thumbnails
				li = document.createElement("li");
	            div = document.createElement("div");
	            center = document.createElement("div");
	            jQuery(center).addClass("centered");
	            jQuery(div).attr("class", "thumbnail");
	            img = document.createElement("img");
	            jQuery(img).attr("src", attachment.url)
	            jQuery(img).addClass(attachment.orientation);
	            jQuery(center).append(img);
	            jQuery(div).append(center);
	            jQuery(li).append(div);
	            jQuery("#slideshow-images").append(li);

	            // Set up form inputs
	            form = jQuery('#nt-bg-form');
	            inp_src = document.createElement('input');
	            jQuery(inp_src).attr('type', 'hidden');
	            jQuery(inp_src).attr('name', 'background_images['+attachment.id+'][src]');
	            jQuery(inp_src).val(attachment.url);
	            form.append(inp_src);
	            inp_or = document.createElement('input');
	            jQuery(inp_or).attr('type', 'hidden');
	            jQuery(inp_or).attr('name', 'background_images['+attachment.id+'][orientation]');
	            jQuery(inp_or).val(attachment.orientation);
	            form.append(inp_or);

	            // Set up slideshow images
				prev_img = document.createElement('img');
				jQuery(prev_img).attr('src', attachment.url);
				jQuery(prev_img).addClass('mc-image');
				jQuery(bg_prev).append(prev_img);
			});
			init_slideshow();
		});

		// Finally, open the modal
		file_frame.open();
	});

	function set_overlay_pattern(name) {
		if(name == "") jQuery('#bg-overlay').css('background-image', 'none');
		jQuery('#bg-overlay').css('background-image', "url('/wp-content/plugins/nt-big-background-slideshow/assets/patterns/"+name+"')");
	}

	function set_overlay_selected() {
		set_overlay_pattern(jQuery('#pattern_name_input').val());
	}

	function set_selected_pattern(elem, name) {
		set_overlay_pattern(name);
		jQuery('.pattern-sample').each(function() {
			jQuery(this).parent().removeClass('selected');
		})
		jQuery(elem).parent().addClass('selected');
		jQuery('#pattern_name_input').val(name);
	}

	function init_slideshow() {
		jQuery(function($) {
			$('#bg-preview').maximage({
				cycleOptions: {
					fx: '<?=$maximage_effect?>',
					speed: 1000,
					timeout: <?=$timeout?>,
					prev: '#arrow_left',
					next: '#arrow_right',
					pause: 1
				},
				fillElement: '#preview_wrapper',
				backgroundSize: 'contain'
			});
		});
	}
	<? if(count($background_images)) : ?>
	init_slideshow();
	<? endif ?>
</script>