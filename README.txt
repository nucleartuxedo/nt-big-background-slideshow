=== NT Big Background Slideshow ===
Tags: slideshow, background, fullscreen
Requires at least: 3.5.1
Tested up to: 3.5.1
Stable tag: 0.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Fullscreen background slideshows with optional overlays.

== Description ==

Fullscreen background slideshows with optional overlays.

== Changelog ==

= 0.0.1 =
* Initial Functionality